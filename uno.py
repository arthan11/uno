from random import shuffle, randint

RED = 1
GREEN = 2
YELLOW = 3
BLUE = 4
COLORS = (RED, GREEN, YELLOW, BLUE)
COLORS_TXT = {1: 'R', 2: 'G', 3: 'Y', 4:'B'}

def color_to_txt(color):
    if color > 0 and color < 5:
        return COLORS_TXT[color]
    else:
        return ''

class Karta(object):
    pass

class KartaUno(Karta):
    def __init__(self, kolor=None, numer=None):
        super(KartaUno, self).__init__()
        self.kolor = kolor
        self.numer = numer

    def __repr__(self):
        txt = ''
        if self.kolor != None:
            txt += color_to_txt(self.kolor)
        if self.numer != None:
            txt += ' {}'.format(self.numer)
        else:
            txt += ' {}'.format(self.__class__.__name__)
        return '<{}>'.format(txt.strip())
        
class KartaUnoSkip(KartaUno):
    pass

class KartaUnoReverse(KartaUno):
    pass
    
class KartaUnoDrawTwo(KartaUno):
    pass
    
class KartaUnoDrawFourWild(KartaUno):
    pass

class KartaUnoWild(KartaUno):
    pass
    
class Talia(object):
    def __init__(self):
        self.karty = []

    def tasuj(self):
        shuffle(self.karty)

    def wez_karte(self):
        return self.karty.pop()

    def poloz_karte(self, karta):
        self.karty.append(karta)

class TaliaUno(Talia):
    def __init__(self):
        super(TaliaUno, self).__init__()
        self.nowa_talia()
        self.tasuj()

    def nowa_talia(self):
        self.karty = []
        for double in range(2):
            for color in COLORS:
                # cyfry
                for i in range(10):
                    karta = KartaUno(color, i)
                    self.karty.append(karta)
                # 8 kart stopu (Skip) po dwie z kazdego koloru
                self.karty.append(KartaUnoSkip(color))
                # 8 kart zmiany kierunku (Reverse) po dwie z kazdego koloru
                self.karty.append(KartaUnoReverse(color))
                # 8 kart +2 (Draw Two) po dwie z kazdego koloru
                self.karty.append(KartaUnoDrawTwo(color))
        for i in range(4):
            # 4 czarne karty +4 ze zmiana koloru (Draw Four Wild)
            self.karty.append(KartaUnoDrawFourWild())
            # 4 czarne karty zmiana koloru (Wild)
            self.karty.append(KartaUnoWild())

class Gracz(object):
    def __init__(self, nazwa):
        self.nazwa = nazwa

class GraczUno(Gracz):
    def __init__(self, nazwa):
        super(GraczUno, self).__init__(nazwa)
        self.reka = Talia()

class Gra(object):
    def __init__(self):
        self.gracze = []
        self.gracz = None
        self.gracze_clockwise = True

    def dodaj_gracza(self, gracz):
        self.gracze.append(gracz)

    def losuj(self, ilosc=1, oczek=6):
        wynik = 0
        for i in range(ilosc):
            wynik += randint(1, oczek)
        return wynik

    def losuj_gracza(self):
        nr_gracza = self.losuj(oczek=len(self.gracze))
        return self.gracze[nr_gracza-1]

    def nastepny_gracz(self):
        gracz_id = self.gracze.index(self.gracz)
        max_id = len(self.gracze) -1
        if self.gracze_clockwise:
            if gracz_id < max_id:
                self.gracz = self.gracze[gracz_id+1]
            else:
                self.gracz = self.gracze[0]
        else:
            if gracz_id > 0:
                self.gracz = self.gracze[gracz_id-1]
            else:
                self.gracz = self.gracze[-1]

class Uno(Gra):
    def __init__(self):
        super(Uno, self).__init__()
        self.talia = TaliaUno()
        self.na_stole = Talia()

    def rozdaj_po_7_kart(self):
        for i in range(7):
            for gracz in self.gracze:
                gracz.reka.poloz_karte(self.talia.wez_karte())

    def oblicz_pkt_za_reke(self, gracz):
        ADD_1 = 20 # 10
        ADD_2 = 50 # 20
        wynik = 0
        for karta in gracz.reka.karty:
            if karta.__class__ in (KartaUnoDrawFourWild, KartaUnoWild):
                wynik += ADD_2
            else:
                if karta.numer != None:
                    wynik += karta.numer
                else:
                    wynik += ADD_1
        return wynik

    '''
    def czy_mozna_polozyc(self, karta):
        if len(self.na_stole.karty) == 0:
            return True
        w_talii = self.na_stole.karty[-1]
        if w_talii.numer != None:
            if w_talii.numer == karta.numer:
                return True
        elif w_talii.__class__ in (KartaUnoSkip, KartaUnoReverse):
            if w_talii.kolor == karta.kolor or karta.kolor == None:
                return True
        elif w_talii.__class__ == :


        if karta.kolor != None:
            if karta.kolor == w_talii.kolor:
                return True
        if karta.kolor == None:
            if w_talii.kolor == None:
                if karta.__class__ == w_talii.__class__:
                    return True
            else:
                return True
        return False
    '''

    def ktore_mozna_wylozyc(self, gracz):
        ret = []
        for karta in gracz.reka.karty:
            if self.czy_mozna_polozyc(karta):
                ret.append(karta)
        return ret

if __name__ == '__main__':
    gra = Uno()
    gra.dodaj_gracza(GraczUno('Marek'))
    gra.dodaj_gracza(GraczUno('Ela'))
    gra.dodaj_gracza(GraczUno('Piotr'))
    gra.gracz = gra.losuj_gracza()
    gra.rozdaj_po_7_kart()
    gra.na_stole.poloz_karte(gra.talia.wez_karte())

    print len(gra.talia.karty)
    print gra.gracz.nazwa
    print gra.gracz.reka.karty
    print gra.na_stole.karty
    print gra.oblicz_pkt_za_reke(gra.gracz)